import 'package:flutter/material.dart';
import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';
import 'package:photo_view/photo_view.dart';

class Resume extends StatefulWidget {
  @override
  _ResumeState createState() => _ResumeState();
}

class _ResumeState extends State<Resume> {
  String resPdf = "assets/resume.pdf";
  String url =
      "https://drive.google.com/file/d/1psbOApkCk1yJ5Y5A5AuzrHZbvmNTAbxy/view";
  PDFDocument _doc;
  bool _loading;
  @override
//  void initState() {
//    super.initState();
//    _initPdf();
//  }
//
//  _initPdf() async {
//    setState(() {
//      _loading = true;
//    });
//    PDFDocument doc = await PDFDocument.fromAsset(resPdf);
//    setState(() {
//      _doc = doc;
//      _loading = false;
//    });
//  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
//              backgroundColor: Colors.transparent,
        title: Text('My Resume'),
        centerTitle: true,
      ),
      body: Container(
          child: PhotoView(
        imageProvider: AssetImage("assets/resume.jpg"),
      )),

//      body: _loading
//          ? Center(
//              child: CircularProgressIndicator(),
//            )
//          : PDFViewer(
//              document: _doc,
//            ),
    );
  }
}
