import 'package:flutter/material.dart';

class About extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 200.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  title: Text("About me",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                      )),
                  background: Image.network(
                    "https://images.pexels.com/photos/169573/pexels-photo-169573.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260",
                    fit: BoxFit.cover,
                  )),
            ),
          ];
        },
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              colorFilter: ColorFilter.mode(
                  Colors.white.withOpacity(0.7), BlendMode.srcOver),
              image: AssetImage("assets/bg2.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 25.0,
              ),
              Padding(
                padding: const EdgeInsets.all(25.0),
                child: Text(
                  'Final year student of B.Tech Computer Science. Previously done 3 yrs diploma in Computer Engineering. Skilled in Core-Java, C++, Database, and Angular 6. With Strong knowledge of Data Structure and Algorithms. Have done internship as a Web developer in Enigma Digital Consulting.',
                  style: TextStyle(
                    fontSize: 18,
                  ),
                  textAlign: TextAlign.justify,
                ),
              ),
              Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.arrow_right,
                        size: 18,
                      ),
                      Text(
                        'B.Tech in CSE  2020',
                        style: TextStyle(fontSize: 20),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 25.0),
                    child: Row(
                      children: <Widget>[
                        Text(
                          'G. B. Pant Engg College, New Delhi',
                          style: TextStyle(fontSize: 18),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.arrow_right,
                        size: 18,
                      ),
                      Text(
                        'Diploma Engg in CSE  2016',
                        style: TextStyle(fontSize: 20),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 25.0),
                    child: Row(
                      children: <Widget>[
                        Text('Jamia Millia Islamia, New Delhi',
                            style: TextStyle(fontSize: 18)),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.arrow_right,
                        size: 18,
                      ),
                      Text(
                        'Internship in Web Dev 2019',
                        style: TextStyle(fontSize: 20),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 25.0),
                    child: Row(
                      children: <Widget>[
                        Text('Enigma Digital Consulting, Noida',
                            style: TextStyle(fontSize: 18)),
                      ],
                    ),
                  ),
//                  SizedBox(height: 50,),
//                  Padding(
//                    padding: const EdgeInsets.only(left:30.0),
//                    child: Row(children: <Widget>[
//                      Icon(
//                        Icons.picture_in_picture,
//                        size: 25,
//                      ),
//                      SizedBox(width: 10,),
//                      Text('Skills',style: TextStyle(fontSize: 25)),
//                    ],),
//                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
